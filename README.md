# Gemini Mentions

**NOTA**: Last update: 2023-01-09

The goal of this document is to details the `Gemini Mentions` standard to facilitate responses between capsule owner through gemlog entries.

For more details/history, please read this gemlog post:
[bacardi55 gemlog post about gemini mentions](gemini://gmi.bacardi55.io/gemlog/2022/02/27/my-take-on-gemlog-replies/)


## Specifications:

Gemini mentions are a way to indicate to another capsule owner that you wrote a response to one of their content on your own capsule.

* To be alerted of mentions, a capsule owner MUST implement a basic endpoint on their capsule. Capsule owner MUST decide the endpoint path.
* This endpoint MUST require 1 argument containing the URL of the response.
* The URL MUST be % encoded.
* The endpoint MUST check for the validity of the page received.
* The endpoint MUST check that the response contains at least one link pointing back to a valid URI.
* A gemini response page MAY contain multiple mention links for the same or different users.
* The first mention link for the current capsule found MUST be notified to the capsule owner.
* The optional other mention links MAY be notified to the capsule owner.
* Capsule owner MAY decide for the link limit they want to impose for their capsule.
* Capsule owner MAY allow gemini mention for all or a subset of the capsule URIs.

## Example implementations

### Server side software

* Bash implementation. Working proof of concept in less than 100 lines of bash code. See the [mentions example script](example/mentions).
* [GGM](https://git.sr.ht/~bacardi55/ggm) (Go Gemini Mentions), a Golang implementation.
