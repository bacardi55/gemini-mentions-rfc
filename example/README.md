# Example

This directory contains bash scripts examples.

## mentions

The `mentions` script is an actual implementation of this RFC. It can be place in your capsule in the `/well-known/` folders (as executable).

This script **requires** [gemget](https://github.com/makeworld-the-better-one/gemget) to be installed to actually send the notification (email). If you wish to send the notification any other way, feel free to edit the `execute_mentions` function.

WARNING: This is a very beta program to implement the gemini mention RFC, use with caution!

Big thanks to [Bollux gemini browser](https://tildegit.org/acdw/bollux/src/branch/main/bollux) for the inspiration and a few lines of code.

## automentions.sh

This is a small script that will extract the gemini mentions found try to send the request to the capsule owner. It has been coded in a few minutes and has some limitation, mainly it only takes the root domain, and won't take into account correctly capsule addresses with a subdirectory after the domain. This can be upgraded later on but I didn't want to spend much time on this script so far.

WARNING: This is a very beta program to implement the gemini mention RFC, use with caution!
